package com.gsiano.geotext;

import java.util.Locale;

import android.util.Log;

public class GTUtils {
	
	public static void LOG(String s) {
		GTUtils.LOG(s, true);
	}
	
	public static void LOG(String s, boolean toUpper) {
		String msg = s;
		if (toUpper)
			msg = s.toUpperCase(Locale.US);
		Log.d("GTANDROID", "GTANDROID: " + msg);
	}
}