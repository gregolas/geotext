package com.gsiano.geotext;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;


public class LocationService implements LocationListener {
	
	private final Context context;
	
	public boolean isGPSEnabled = false;
	public boolean isNetworkEnabled = false;
	public boolean canGetLocation = false;
	
	Location location;
	double latitude, longitude;
	
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
	private static final long MIN_TIME_BTW_UPDATES = 1000 * 60 * 1;
	
	protected LocationManager locationManager;
	
	public LocationService(Context context) {
		this.context = context;
		getLocation();
	}
	
	public Location getLocation() {
		// FOR DEBUG PURPOSES ONLY
		//
		
		try {
			locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
			isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
			GTUtils.LOG("GPS IS ENABLED: " + isGPSEnabled);
			isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			GTUtils.LOG("NETWORK IS ENABLED: " + isNetworkEnabled);
			
			if (!isGPSEnabled && !isNetworkEnabled) {
				// get fucked..
			} else {
				this.canGetLocation = true;
				if (isNetworkEnabled) {
					GTUtils.LOG("network location is enabled");
					locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
							MIN_TIME_BTW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
					GTUtils.LOG("USING NETWORK FOR LOCATION");
					if (locationManager != null) {
						location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						if (location != null) {
							latitude = location.getLatitude();
							longitude = location.getLongitude();
						}
					}
				}
				else if (isGPSEnabled) {
					GTUtils.LOG("GPS location is enabled");
					if (location == null) {
						locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
								MIN_TIME_BTW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
						GTUtils.LOG("using GPS for location");
						if (locationManager != null) {
							location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
							if (location != null) {
								latitude = location.getLatitude();
								longitude = location.getLongitude();
							}
						}
					}
				}
			}
		} catch (Exception e) {
			GTUtils.LOG("fuckkkkkkkkkkkkkkkkk");
		}
		return location;
	}
	
	public void stopGPS() {
		if (locationManager != null) {
			locationManager.removeUpdates(LocationService.this);
		}
	}
	
	public double getLatitude() {
		if (location != null) {
			latitude = location.getLatitude();
		}
		return latitude;
	}
	
	public double getLongitude() {
		if (location != null) {
			longitude = location.getLongitude();
		}
		return longitude;
	}
	
	public boolean canGetLocation() {
		return this.canGetLocation;
	}

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}

}
