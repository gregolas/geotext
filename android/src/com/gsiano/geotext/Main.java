package com.gsiano.geotext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.app.Activity;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class Main extends Activity {
	
	LocationService loc;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button b = (Button)findViewById(R.id.button1);
		Button b2 = (Button)findViewById(R.id.button2);
		loc = new LocationService(this);
		b.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				new HttpAsyncTask().execute("http://184.73.231.146:7890");
			}
		});
		b2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Location l = loc.getLocation();
				if (l != null)
					showToast(l.getLatitude() + ", " + l.getLongitude());
				else
					showToast("no location!");
			}
		});
	}
	
	double distance(Location l1, Location l2) {
		double R = 6371; //km
		double dLat = Math.toRadians((l2.getLatitude() - l1.getLatitude()));
		double dLon = Math.toRadians((l2.getLongitude() - l1.getLongitude()));
		double lat1 = Math.toRadians(l1.getLatitude());
		double lat2 = Math.toRadians(l2.getLatitude());
		
		double a = Math.sin(dLat/2) * Math.sin(dLat / 2) +
				   Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double d = R * c;
		return d * 1000;		// m
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public String POST(String url, String[] data){
		InputStream inputStream;
        String result = "";
        try {
        		HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
 
            String json = "";
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("number", data[0]);
            jsonObject.accumulate("msg", data[1]);
            json = jsonObject.toString();
            StringEntity se = new StringEntity(json);

            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
 
            HttpResponse httpResponse = httpclient.execute(httpPost);
            inputStream = httpResponse.getEntity().getContent();
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "http POST failed.";
            showToast("result: " + result);
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
            showToast("http POST error");
        }
        return result;
    }
	
	private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
 
    }
	
	private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
        		String[] data = {"14083149094", "android!"};
            return POST(urls[0], data);
        }
        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(getBaseContext(), "Data Sent!", Toast.LENGTH_LONG).show();
       }
    }
	
	public void showToast(final String s) { showToast(s, Toast.LENGTH_LONG); }
	
	public void showToast(final String s, final int duration) {
		runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(Main.this, s, duration).show();
			}
		});
	}
}