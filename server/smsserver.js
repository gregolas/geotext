var secrets = require(process.env['HOME'] + "/.secrets.js");
var http = require('http');
var twilio = require("twilio");
var client = new twilio.RestClient(accountSid, authToken);
var PORT = 7890;

if (process.argv[2])
    PORT = process.argv[2];

function sendMsg(number, msg) {
    if (number.length != 10 || number.charAt(0) != 1)
        return -1;
    client.sms.messages.create({
            body: msg,
            to: "+" + number,
            from: phoneNumber
    }, function(err, message) {
        if (err)
            console.log(err);
        process.stdout.write(message.sid);
    });
    return 1;
}

http.createServer(function(request, response) {
    console.log('received request');
    if (request.method == 'POST') {
        console.log("POST");
        var data = '';
        request.on('data', function(chunk) {
            data += chunk;
        });
        request.on('end', function() {
            response.writeHead(200, {"Content-Type": "text/plain"});
            response.write('thanks!');
            response.end();
            console.log("data: " + data);
            var json = JSON.parse(data);
            console.log(json);
            var number = json['number'];
            var msg = json['msg'];
            console.log(number + ": " + msg);
            sendMsg(number, msg);
        });
    }
    else {
        console.log("NOT POST");
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.write("HTTP POST required!");
        response.end();
    }
}).listen(PORT);

console.log("listening on port " + PORT);
